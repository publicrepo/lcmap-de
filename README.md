# LCMAP-DE

This is the public repository for LCMAP-DE.

**Download**

Please download the installation tool [here](https://www.dropbox.com/s/1lk2hz4l3ec2x20/setup.jar?dl=0).

**Installation**

Run `java -jar setup.sh`

**Running LCMAP-DE**

In the installation folder run `./start_model.command`

**Contact**

Leonardo Moreira - leohmoreira@gmail.com